package pk.osgi.project.pomocnikgracza.GamingTime.internal;

import de.ksquared.system.keyboard.GlobalKeyListener;
import de.ksquared.system.mouse.GlobalMouseListener;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import pk.osgi.project.pomocnikgracza.Contract.*;

/**
 * Extension of the default OSGi bundle activator
 */
public final class GamingTimeActivator implements BundleActivator {
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    
    ServiceReference stoperRef;
    
    GlobalMouseListener gml;
    MouseCheck mc;
    GlobalKeyListener gkl;
    KeyboardCheck kc;
    
    public void start( BundleContext bc ) throws Exception {
        System.out.println( "Startowanie GamingTime!" );
        
        stoperRef = bc.getServiceReference(IStoper.class.getName());
        IStoper stoper =(IStoper)bc.getService(stoperRef);
        
        gml = new GlobalMouseListener();
        mc = new MouseCheck(gml);
        mc.SluchajMyszy();
    
        gkl = new GlobalKeyListener();
        kc = new KeyboardCheck(gkl);
        kc.SluchajKlawy();
        
        GamingTime gt = new GamingTime(stoper);
        gt.mc = mc;
        gt.kc = kc;
        
        // Register our example service implementation in the OSGi service registry
        bc.registerService( IGamingTime.class.getName(), gt, null );
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop( BundleContext bc ) throws Exception {
        System.out.println( "Ladowanie GamingTime!" );
        mc.NieSluchajMyszy();
        kc.NieSluchajKlawy();
    }
}

