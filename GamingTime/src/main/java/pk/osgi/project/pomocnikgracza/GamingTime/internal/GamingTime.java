/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.GamingTime.internal;

import de.ksquared.system.keyboard.GlobalKeyListener;
import de.ksquared.system.mouse.GlobalMouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import pk.osgi.project.pomocnikgracza.Contract.IStoper;
import pk.osgi.project.pomocnikgracza.Contract.IGamingTime;

/**
 *
 * @author MadMike
 */
public class GamingTime implements IGamingTime, Runnable {

    private IStoper stoper;
    private long czas;
    public MouseCheck mc;
    public KeyboardCheck kc;
    private List<String> listaProcesow = new ArrayList();

    public void setListaProcesow(List<String> listaProcesow) {
        this.listaProcesow = listaProcesow;
    }
    
    public GamingTime(IStoper stoper) {
        this.stoper = stoper;
        this.czas = 0;
    }
    
    @Override
    public void LiczCzasGrania() {
        
        Process p = null;
        
        try {
            p = new ProcessBuilder("tasklist").start();
            InputStream is = p.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            Boolean czyjest = false;
            while ((line = br.readLine()) != null) {
                    if(line.contains("firefox.exe")) {
                        czyjest = true;
                        break;
                    }
                    else {
                        czyjest = false;
                    }
            }
            
            if(czyjest == true && (mc.czyUzywana == true || kc.czyUzywana == true) && stoper.getPracuje() == false) { 
                System.out.println("Start :");
                 stoper.Start();
            }
            else if (czyjest == true && mc.czyUzywana == false && kc.czyUzywana == false && stoper.getPracuje() == true) {
                czas = stoper.Stop();
                System.out.println("Czas trwanie aplikacji : " + czas + " s");
            }
            else if(czyjest == false && stoper.getPracuje() == true) {
                czas = stoper.Stop();
                System.out.println("Czas trwanie aplikacji : " + czas + " s");
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }
    }
    
    @Override
    public void run() {
        
        Thread mcThread = new Thread(mc);
        mcThread.start();
        
        Thread kcThread = new Thread(kc);
        kcThread.start();
        
        while(true) {
            LiczCzasGrania();
        }
    }  
}