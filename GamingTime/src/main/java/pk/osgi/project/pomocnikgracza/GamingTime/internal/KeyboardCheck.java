/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.GamingTime.internal;

import de.ksquared.system.keyboard.GlobalKeyListener;
import de.ksquared.system.keyboard.KeyAdapter;
import de.ksquared.system.keyboard.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MadMike
 */
public class KeyboardCheck implements Runnable {
    
    GlobalKeyListener gkl;
    KeyEvent klawisz;
    Boolean czyUzywana;
    KeyAdapter ka;
    
    public KeyboardCheck(GlobalKeyListener gkl) {
        this.gkl = gkl;
    }
    
    public void SluchajKlawy() {
        ka = new KeyAdapter() {
            
            @Override public void keyPressed(KeyEvent event) {}  
            @Override public void keyReleased(KeyEvent event) { klawisz = event; }
        };
        gkl.addKeyListener(ka);
    }
    
     public void NieSluchajKlawy() {
         gkl.removeKeyListener(ka);
    }

    @Override
    public void run() {
        while (true) {
            KeyEvent temp = klawisz;
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MouseCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (temp == klawisz) {
                czyUzywana = false;
            }
            else {
                czyUzywana = true;
            }
        }
    }
}
