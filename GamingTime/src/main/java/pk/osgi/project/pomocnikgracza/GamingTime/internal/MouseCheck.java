/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.GamingTime.internal;

import de.ksquared.system.mouse.GlobalMouseListener;
import de.ksquared.system.mouse.MouseAdapter;
import de.ksquared.system.mouse.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MadMike
 */
public class MouseCheck implements Runnable {
    
    GlobalMouseListener gml;
    MouseEvent wspolrzedne;
    Boolean czyUzywana;
    MouseAdapter ma;
    
    public MouseCheck(GlobalMouseListener gml) {
        this.gml = gml;
        this.czyUzywana = true;
    }
    
    public void SluchajMyszy() {
        ma = new MouseAdapter() {
            
            @Override public void mousePressed(MouseEvent event) {}   
            @Override public void mouseReleased(MouseEvent event) {}    
            @Override public void mouseMoved(MouseEvent event) { wspolrzedne = event; }
        };
        gml.addMouseListener(ma);
    }
    
        public void NieSluchajMyszy() {
        gml.removeMouseListener(ma);
    }

    @Override
    public void run() {
        while (true) {
            MouseEvent temp = wspolrzedne;
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MouseCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (temp == wspolrzedne) {
                czyUzywana = false;
            }
            else {
                czyUzywana = true;
            }
        }
    }  
}
