/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.Contract;

import java.io.File;
import javax.swing.JList;

/**
 *
 * @author MadMike
 */
public interface IBackup {
    File[] finder( String dirName);
    void aktualizacja_listy_prawej(JList listbox);
    void aktualizacja_listy_lewej(JList listbox);
    void kopiowanie_pliku_w_lewo(JList listbox);
    void kopiowanie_pliku_w_prawo(JList listbox);
    void setFolder_gry(String folder_gry);
    void setFolder_gracza(String folder_gracza);
}
