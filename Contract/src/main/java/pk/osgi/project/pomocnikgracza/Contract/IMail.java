/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.Contract;

/**
 *
 * @author MadMike
 */
public interface IMail {
    void WysliMailaBezZalacznika(String adresOdbiorcy, String adresNadawcy, String HasloNadawcyDoMeila, String Temat, String Tresc);
    void WyslijMailaZZalacznikiem(String adresOdbiorcy, String adresNadawcy, String HasloNadawcyDoMeila, String Temat, String Tresc, String SciezkaDoPliku);
}
