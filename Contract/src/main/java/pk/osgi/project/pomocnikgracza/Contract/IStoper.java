package pk.osgi.project.pomocnikgracza.Contract;

/**
 * Public API representing an example OSGi service
 */
public interface IStoper {
    void Start();
    long Stop();
    boolean getPracuje();
}

