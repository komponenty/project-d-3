package pk.osgi.project.pomocnikgracza.Contract.internal;


import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


/**
 * Extension of the default OSGi bundle activator
 */
public final class ContractActivator implements BundleActivator {
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    public void start( BundleContext bc ) throws Exception {
        System.out.println("Startowanie Contract!");
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop( BundleContext bc ) throws Exception {
        System.out.println("Ladowanie Contract!");
    }
}

