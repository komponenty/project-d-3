/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.Contract;

import java.sql.SQLException;
import java.sql.Time;

/**
 *
 * @author MadMike
 */
public interface IDataBase {
    void DoConnect(String nickGracza) throws SQLException;
    void WpiszNowegoUzytkownika(String Nick);
    void WpiszGreDoUzytkownika(String Nick, String NazwaGry);
    void UstawCalkowityCzasGryDlaAktualnegoGracza(Time czas, String NazwaGry);
    void DodajCzasGryDlaAktualnegoGracza(Time DodatkowyCzas, String NazwaGry);
    void ZwrocWszystkieGry();
    void ZwrocWszystkieGryAktualnegoGracza();
    void ZwrocPegiDlaDanejGry(String NazwaGry);
    void ZwrocAchievementyDanejGry(String nazwaGry);
    
}
