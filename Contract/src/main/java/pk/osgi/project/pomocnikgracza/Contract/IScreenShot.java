/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.Contract;

import java.awt.AWTException;
import java.io.IOException;

/**
 *
 * @author MadMike
 */
public interface IScreenShot {
    void SluchajKlawy();
    void NieSluchajKlawy();
    void ZapiszScreenShot() throws AWTException, IOException;
}
