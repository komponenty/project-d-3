package pk.osgi.project.pomocnikgracza.Mail.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.osgi.project.pomocnikgracza.Contract.IMail;

/**
 * Extension of the default OSGi bundle activator
 */
public final class MailActivator implements BundleActivator {
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    public void start( BundleContext bc ) throws Exception {
        System.out.println( "Startowanie Mail!" );

        // Register our example service implementation in the OSGi service registry
        Mail mail = new Mail();
        bc.registerService( IMail.class.getName(), mail, null );
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop( BundleContext bc ) throws Exception {
        System.out.println( "Ladowanie Mail!" );
    }
}

