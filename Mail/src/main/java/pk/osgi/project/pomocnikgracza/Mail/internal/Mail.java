/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.osgi.project.pomocnikgracza.Mail.internal;

import pk.osgi.project.pomocnikgracza.Contract.IMail;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
/**
 *
 * @author Bartek
 */
public class Mail implements IMail {
    
    public Mail() {
        
    }
    
    @Override
    public void WysliMailaBezZalacznika(String adresOdbiorcy, String adresNadawcy, String HasloNadawcyDoMeila, String Temat, String Tresc)
    {
       final String  nadawca = adresNadawcy;
       final String haslodomeila = HasloNadawcyDoMeila;
       String to = adresOdbiorcy;
        
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(nadawca, haslodomeila);
            }
        });
        
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(nadawca));//change accordingly  
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(Temat);
            message.setText(Tresc);

            //send message  
            Transport.send(message);

            //System.out.println("wyslano");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public void WyslijMailaZZalacznikiem(String adresOdbiorcy, String adresNadawcy, String HasloNadawcyDoMeila, String Temat, String Tresc, String SciezkaDoPliku) {

        final String user = adresNadawcy;
        final String password = HasloNadawcyDoMeila;
        String to = adresOdbiorcy;


        //1) get the session object     
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", "mail.javatpoint.com");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");

        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        });

        //2) compose message     
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(Temat);

            //3) create MimeBodyPart object and set your message text     
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText(Tresc);

            //4) create new MimeBodyPart object and set DataHandler object to this object      
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();

            String filename = SciezkaDoPliku;//change accordingly  
            DataSource source = new FileDataSource(filename);
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(filename);


            //5) create Multipart object and add MimeBodyPart objects to this object      
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);
            multipart.addBodyPart(messageBodyPart2);

            //6) set the multiplart object to the message object  
            message.setContent(multipart);

            //7) send message  
            Transport.send(message);

            //System.out.println("wyslano");
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }
    
}
