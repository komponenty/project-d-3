/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.GUI.internal;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import pk.glk.soundplayer.ISound;
import pk.glk.soundplayer.internal.SoundPlayer;

/**
 *
 * @author MadMike
 */
public class NickChooser extends javax.swing.JFrame {

    /**
     * Creates new form NickChoose
     */
    private String nick = "";
    private DataBase dataBase;
    private ISound sound = new SoundPlayer();

    public void setDataBase(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    public String getNick() {
        return nick;
    }
   
    public NickChooser() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        button_loguj = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        button_stworz = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Podaj sw�j nick :");

        button_loguj.setText("Loguj");
        button_loguj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_logujActionPerformed(evt);
            }
        });

        jLabel2.setText("Stw�rz nowego u�ytkownika :");

        button_stworz.setText("Stw�rz");
        button_stworz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_stworzActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(68, 68, 68)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel2))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jTextField2, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(button_stworz, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(button_loguj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(button_loguj, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(button_stworz, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void button_logujActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_logujActionPerformed
        nick = jTextField1.getText();
        Boolean czyIstnieje;
        try {
            czyIstnieje = dataBase.DoConnectDlaIstniejacegoUzytkownika(nick);
            if (dataBase.czyPolaczony) {
                if (!czyIstnieje) {
                    sound.PlaySound("sound/windows_error.wav", false);
                    JOptionPane.showMessageDialog(null, "Nie ma u�ytkownika o takim nicku!", "Uwaga!", JOptionPane.OK_OPTION);
                    jTextField1.setText("");
                }
                else {
                    this.dispose();
                }
            }
            else {
                sound.PlaySound("sound/windows_error.wav", false);
                JOptionPane.showMessageDialog(null, "Brak po��czenia z internetem. Jest ono wymagane do poprawnej pracy programu!", "Uwaga!", JOptionPane.OK_OPTION);
                jTextField1.setText("");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(NickChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_button_logujActionPerformed

    private void button_stworzActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_stworzActionPerformed
        nick = jTextField2.getText();
        try {
            Boolean czyStworzony = dataBase.DoConnectUtworzNowegoUzytkownika(nick);
            if (dataBase.czyPolaczony) {
                if (czyStworzony) {
                    Boolean a = dataBase.DoConnectDlaIstniejacegoUzytkownika(nick);
                    this.dispose();
                }
                else {
                    sound.PlaySound("sound/windows_error.wav", false);
                    JOptionPane.showMessageDialog(null, "Taki nick ju� istnieje!", "Uwaga!", JOptionPane.OK_OPTION);
                    jTextField1.setText("");
                }
            }
            else {
                sound.PlaySound("sound/windows_error.wav", false);
                JOptionPane.showMessageDialog(null, "Brak po��czenia z internetem. Jest ono wymagane do poprawnej pracy programu!", "Uwaga!", JOptionPane.OK_OPTION);
                jTextField1.setText("");
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(NickChooser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(NickChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_button_stworzActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NickChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NickChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NickChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NickChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NickChooser().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton button_loguj;
    private javax.swing.JButton button_stworz;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables

}
