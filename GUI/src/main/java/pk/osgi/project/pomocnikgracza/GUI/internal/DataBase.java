/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.osgi.project.pomocnikgracza.GUI.internal;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Bartek
 */
public class DataBase {

    private Connection con;
    private Statement stmt1;
    private Statement stmt2;
    private Statement stmt3;
    private Statement stmt4;
    private Statement stmt5;
    private Statement stmt6;
    private ResultSet rsNick;
    private ResultSet rsWszystkieNicki;
    private ResultSet rsGry;
    private ResultSet rsGryGracza;
    private ResultSet rsGryPegi;
    private ResultSet rsGraAchievementGracz;
    private int curRow = 0;
    public List<Game> ListaWszystkichGier;
    public List<Game> ListaGierGracza;
    public PEGI PegiOstatnioPobranejGry;
    public List<String> ListaWszystkichNickow;
    public Boolean czyPolaczony = true;

    public Boolean DoConnectUtworzNowegoUzytkownika(String nickGracza) throws ClassNotFoundException
    {
        Boolean czyStworzony = true;
        czyPolaczony = true;
        try {
            String host = "jdbc:mysql://db4free.net:3306/grypk"; // adres bazy, pasy i login
            String userName = "bartekwojcik";
            String userPassword = "plokij";
                
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(host, userName, userPassword);
            stmt1 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); //spos�b przegladania bazy, nie istotne jako� szczegolnie
            String SQL = "Select * from Nicki";     
            rsNick = stmt1.executeQuery(SQL); //wykonanie komendy
            
            List<String> listaNickow = ZwrocWszystkieNickiWBazie();
            if (!listaNickow.contains(nickGracza)) {
                WpiszNowegoUzytkownika(nickGracza);
            }
            else {
                czyStworzony = false;
            }      
        } catch (SQLException ex) {
            //Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
            czyPolaczony = false;
        }
        return czyStworzony;
    }
    
    public Boolean DoConnectDlaIstniejacegoUzytkownika(String nickGracza) throws SQLException {
        
        Boolean czyIstnieje = true;
        czyPolaczony = true;
        try {
            String host = "jdbc:mysql://db4free.net:3306/grypk"; // adres bazy, pasy i login
            String userName = "bartekwojcik";
            String userPassword = "plokij";
            
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(host, userName, userPassword);
            
            List<String> listaNickow = ZwrocWszystkieNickiWBazie();
            if (listaNickow.contains(nickGracza)) {
                stmt1 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); //sposób przegladania bazy, nie istotne jakoś szczegolnie
                //jesli nickGracza istnieje w bazie to zwroci ten sam nick, jesli nie, to wyskoczy SQLException
                String SQL = "Select * from Nicki where NICK='" + nickGracza + "'";
                rsNick = stmt1.executeQuery(SQL); //wykonanie komendy

                stmt2 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                SQL = "Select * from Gry";
                rsGry = stmt2.executeQuery(SQL);

                stmt3 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                SQL = "Select * from GRAUZYTKOWNIKOW where NICK='" + nickGracza + "'";
                rsGryGracza = stmt3.executeQuery(SQL);

                stmt4 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); //sposób przegladania bazy, nie istotne jakoś szczegolnie
                SQL = "Select * from Grapegi";
                rsGryPegi = stmt4.executeQuery(SQL);

                stmt5 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); //sposób przegladania bazy, nie istotne jakoś szczegolnie
                SQL = "Select * from GraAchievement Where Gracz='" + nickGracza + "'";
                rsGraAchievementGracz = stmt5.executeQuery(SQL);
            }
            else {
                czyIstnieje = false;
            }      
        } catch (SQLException error) {
            //System.out.printf(error.toString());
            czyPolaczony = false;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return czyIstnieje;
    }

    public void WpiszNowegoUzytkownika(String Nick) {
        try {

            rsNick.moveToInsertRow();
            rsNick.updateString("NICK", Nick);

            rsNick.insertRow();

        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void WpiszGreDoUzytkownika(String Nick, String NazwaGry) {
        try {

            rsGryGracza.moveToInsertRow();
            rsGryGracza.updateString("NICK", Nick);
            rsGryGracza.updateString("NAZWAGRY", NazwaGry);
            rsGryGracza.updateTime("CZASGRY", new Time(0));
            rsGryGracza.insertRow();

            rsGraAchievementGracz.moveToInsertRow();
            rsGraAchievementGracz.updateString("NazwaGry", NazwaGry);
            rsGraAchievementGracz.updateString("Gracz", Nick);


            for (int i = 1; i <= 12; i++) {
                rsGraAchievementGracz.updateBoolean("achievement" + i, false);
            }

            rsGraAchievementGracz.insertRow();

        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * ustawia ostateczny czas (calkowity)
     *
     * @param czas
     * @param NazwaGry
     * @param Nick
     */
    public void UstawCalkowityCzasGryDlaAktualnegoGracza(Time czas, String NazwaGry) {
        //      try {
        //          String sql = "Select * from GRAUZYTKOWNIKOW where Nick='"+Nick+"' and NAZWAGRY='"+NazwaGry+"'";
        //          rsGryGracza = stmt.executeQuery(sql);
        //          rsGryGracza.next();
        //          rsGryGracza.updateTime("CZASGRY", czas);
        //          rsGryGracza.updateRow();
        //      } catch (SQLException ex) {
        //          Logger.getLogger(Operacje.class.getName()).log(Level.SEVERE, null, ex);
        //      }

        try {

            while (rsGryGracza.next()) {
                if (rsGryGracza.getString("NAZWAGRY").equals(NazwaGry)) {
                    rsGryGracza.updateTime("CZASGRY", czas);
                    rsGryGracza.updateRow();

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

public void DodajCzasGryDlaAktualnegoGracza(Time DodatkowyCzas, String NazwaGry) {
        //  String sql = "Select * from GRAUZYTKOWNIKOW where Nick='"+Nick+"' and NAZWAGRY='"+NazwaGry+"'";
//          
//          rsGryGracza = stmt.executeQuery(sql);
//          rsGryGracza.next();
//          Time obecnyczas = rsGryGracza.getTime("CZASGRY");
//          long jeden=  obecnyczas.getTime();
//          long dwa= DodatkowyCzas.getTime();
//          long time = jeden + dwa;
//          DodatkowyCzas.setTime(time);
//          rsGryGracza.updateTime("CZASGRY", DodatkowyCzas);
//          rsGryGracza.updateRow();
        try {
            rsGryGracza.first();
             if (rsGryGracza.getString("NAZWAGRY").equals(NazwaGry)) {
                    Time obecnyczas = rsGryGracza.getTime("CZASGRY");
                    long jeden = obecnyczas.getTime();
                    long dwa = DodatkowyCzas.getTime();
                    long time = jeden + dwa;
                    DodatkowyCzas.setTime(time);
                    rsGryGracza.updateTime("CZASGRY", DodatkowyCzas);
                    rsGryGracza.updateRow();

                }
            while (rsGryGracza.next()) {
                if (rsGryGracza.getString("NAZWAGRY").equals(NazwaGry)) {
                    Time obecnyczas = rsGryGracza.getTime("CZASGRY");
                    long jeden = obecnyczas.getTime();
                    long dwa = DodatkowyCzas.getTime();
                    long time = jeden + dwa;
                    DodatkowyCzas.setTime(time);
                    rsGryGracza.updateTime("CZASGRY", DodatkowyCzas);
                    rsGryGracza.updateRow();

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * pobiera okladke z table 'GRY'
     *
     * @param nazwaKolumnyWbazie
     * @return
     * @throws SQLException
     * @throws IOException
     */
    private ImageIcon PobierzZbazyObrazBlob(String nazwaKolumnyWbazie) throws SQLException, IOException {

        InputStream binaryStream = rsGry.getBinaryStream(nazwaKolumnyWbazie); //pobranie pliku binarnego z kolumny
        Image image = ImageIO.read(binaryStream); //zamiana binarneog na obrazek
        ImageIcon icon = new ImageIcon(image); // zamiana obazka na ikone

        return icon;
    }
    
 public List<String> ZwrocWszystkieNickiWBazie() throws ClassNotFoundException {

        try {
            
            List<String> listanickow;
            listanickow = new ArrayList<String>();

            stmt6 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); //spos�b przegladania bazy, nie istotne jako� szczegolnie
            String SQL = "Select * from Nicki";
            rsWszystkieNicki = stmt6.executeQuery(SQL);
            rsWszystkieNicki.first();

            String nick = rsWszystkieNicki.getString("NICK");
            listanickow.add(nick);
            while (rsWszystkieNicki.next()) {
                nick = rsWszystkieNicki.getString("NICK");
                listanickow.add(nick);
            }

            return listanickow;
            
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

  public List<Game> ZwrocWszystkieGry() {
      List<Game> listagier = new ArrayList<Game>();     
      try {

            rsGry.first();
            String nazwa = rsGry.getString("NAZWA");
            Date datawydania = rsGry.getDate("DATAPREMIERY");
            String trybgry = rsGry.getString("TRYBGRY");
            ImageIcon okladka = PobierzZbazyObrazBlob("OKLADKA");
            String proces = rsGry.getString("NAZWAPROCESU");

            String sciezka = rsGry.getString("SciezkaDoSavow");
            Game nowagra = new Game(nazwa, datawydania, trybgry, okladka, proces, sciezka);
            listagier.add(nowagra);

            while (rsGry.next()) {
                nazwa = rsGry.getString("NAZWA");
                datawydania = rsGry.getDate("DATAPREMIERY");
                trybgry = rsGry.getString("TRYBGRY");
                okladka = PobierzZbazyObrazBlob("OKLADKA");
                proces = rsGry.getString("NAZWAPROCESU");
                sciezka = rsGry.getString("SciezkaDoSavow");
                nowagra = new Game(nazwa, datawydania, trybgry, okladka, proces, sciezka);
                listagier.add(nowagra);
            }

        } catch (SQLException | IOException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listagier;
    }

   public List<Game> ZwrocWszystkieGryAktualnegoGracza() {

       List<Game> listagier = new ArrayList<Game>();
        try {
            
            List<String> listanazwagiergracza;
            listanazwagiergracza = new ArrayList<String>();

            rsGry.first();
            rsGryGracza.first();
            if (!(rsGryGracza == null)) {
                String nazwa = rsGryGracza.getString("NAZWAGRY");
            listanazwagiergracza.add(nazwa);

            while (rsGryGracza.next()) {
                nazwa = rsGryGracza.getString("NAZWAGRY");
                listanazwagiergracza.add(nazwa);
            }
            rsGry.first();
            rsGryGracza.first();

            String nazwa1 = rsGry.getString("NAZWA");
            String nazwa2 = rsGryGracza.getString("NAZWAGRY");
            for (String nazwagrygracza : listanazwagiergracza) {
                if (nazwa1.equals(nazwagrygracza)) {
                    nazwa = rsGry.getString("NAZWA");
                    Date datawydania = rsGry.getDate("DATAPREMIERY");
                    String trybgry = rsGry.getString("TRYBGRY");
                    ImageIcon okladka = PobierzZbazyObrazBlob("OKLADKA");
                    String proces = rsGry.getString("NAZWAPROCESU");
                    String sciezka = rsGry.getString("SciezkaDoSavow");
                    Game nowagra = new Game(nazwa, datawydania, trybgry, okladka, proces, sciezka);
                    listagier.add(nowagra);
                }

            }

            while (rsGry.next()) {
                nazwa1 = rsGry.getString("NAZWA");
                for (String nazwagrygracza : listanazwagiergracza) {
                    if (nazwa1.equals(nazwagrygracza)) {
                        nazwa = rsGry.getString("NAZWA");
                        Date datawydania = rsGry.getDate("DATAPREMIERY");
                        String trybgry = rsGry.getString("TRYBGRY");
                        ImageIcon okladka = PobierzZbazyObrazBlob("OKLADKA");
                        String proces = rsGry.getString("NAZWAPROCESU");
                        String sciezka = rsGry.getString("SciezkaDoSavow");
                        Game nowagra = new Game(nazwa, datawydania, trybgry, okladka, proces, sciezka);
                        listagier.add(nowagra);
                    }
                }
            }
            } 
        } catch (SQLException | IOException ex) {
            //Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listagier;
    }
   
    public long ZwrocCzasGraniaWDaneGreDlaZalogowanegoUzytkownika(String nazwaGry) {
        try {
            rsGryGracza.first();
            String nazwa = rsGryGracza.getString("NAZWAGRY");
            if (nazwa.equals(nazwaGry)) {
                long czas = rsGryGracza.getLong("CZASGRY");
                return czas;

            }
            while (rsGryGracza.next()) {
                nazwa = rsGryGracza.getString("NAZWAGRY");
                if (nazwa.equals(nazwaGry)) {
                    long czas = rsGryGracza.getLong("CZASGRY");
                    return czas;
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    /**
     * public Boolean Violence; public Boolean Language; public Boolean Fear;
     * public Boolean Sex; public Boolean Drugs; public Boolean Discrimination;
     * public Boolean Gambling; public Boolean Online; public int Wiek;
     *
     * @param NazwaGry
     * @return
     */
    public PEGI ZwrocPegiDlaDanejGry(String NazwaGry) {
        
        PEGI pegi = new PEGI();
        try {

            rsGryPegi.first();
            if (rsGryPegi.getString("GRA").equals(NazwaGry)) {
                String nazwa = rsGryPegi.getString("GRA");
                Boolean Violence = rsGryPegi.getBoolean("Violence");
                Boolean Language = rsGryPegi.getBoolean("Language");
                Boolean Fear = rsGryPegi.getBoolean("Fear");
                Boolean Sex = rsGryPegi.getBoolean("Sex");
                Boolean Drugs = rsGryPegi.getBoolean("Drugs");
                Boolean Discrimination = rsGryPegi.getBoolean("Discrimination");
                Boolean Gambling = rsGryPegi.getBoolean("Gambling");
                Boolean Online = rsGryPegi.getBoolean("Online");
                int wiek = rsGryPegi.getInt("WIEK");

                pegi = new PEGI(nazwa, Violence, Language, Fear, Sex, Drugs, Discrimination, Gambling, Online, wiek);
            }
            while (rsGryPegi.next()) {
                if (rsGryPegi.getString("GRA").equals(NazwaGry)) {
                    String nazwa = rsGryPegi.getString("GRA");
                    Boolean Violence = rsGryPegi.getBoolean("Violence");
                    Boolean Language = rsGryPegi.getBoolean("Language");
                    Boolean Fear = rsGryPegi.getBoolean("Fear");
                    Boolean Sex = rsGryPegi.getBoolean("Sex");
                    Boolean Drugs = rsGryPegi.getBoolean("Drugs");
                    Boolean Discrimination = rsGryPegi.getBoolean("Discrimination");
                    Boolean Gambling = rsGryPegi.getBoolean("Gambling");
                    Boolean Online = rsGryPegi.getBoolean("Online");
                    int wiek = rsGryPegi.getInt("WIEK");

                    pegi = new PEGI(nazwa, Violence, Language, Fear, Sex, Drugs, Discrimination, Gambling, Online, wiek);
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pegi;
    }

     public Achievements ZwrocAchievementyDanejGry(String nazwaGry) {
         Achievements acziwmenty = new Achievements();
         try {

            rsGraAchievementGracz.first();
            if (rsGraAchievementGracz.getString("NazwaGry").equals(nazwaGry)) {
                Boolean acz1 = rsGraAchievementGracz.getBoolean("achievement1");
                Boolean acz2 = rsGraAchievementGracz.getBoolean("achievement2");
                Boolean acz3 = rsGraAchievementGracz.getBoolean("achievement3");
                Boolean acz4 = rsGraAchievementGracz.getBoolean("achievement4");
                Boolean acz5 = rsGraAchievementGracz.getBoolean("achievement5");
                Boolean acz6 = rsGraAchievementGracz.getBoolean("achievement6");
                Boolean acz7 = rsGraAchievementGracz.getBoolean("achievement7");
                Boolean acz8 = rsGraAchievementGracz.getBoolean("achievement8");
                Boolean acz9 = rsGraAchievementGracz.getBoolean("achievement9");
                Boolean acz10 = rsGraAchievementGracz.getBoolean("achievement10");
                Boolean acz11 = rsGraAchievementGracz.getBoolean("achievement11");
                Boolean acz12 = rsGraAchievementGracz.getBoolean("achievement12");

                acziwmenty = new Achievements(nazwaGry, acz1, acz2, acz3, acz4, acz5, acz6, acz7, acz8, acz9, acz10, acz11, acz12);
            }
            while (rsGraAchievementGracz.next()) {

                if (rsGraAchievementGracz.getString("NazwaGry").equals(nazwaGry)) {
                    Boolean acz1 = rsGraAchievementGracz.getBoolean("achievement1");
                    Boolean acz2 = rsGraAchievementGracz.getBoolean("achievement2");
                    Boolean acz3 = rsGraAchievementGracz.getBoolean("achievement3");
                    Boolean acz4 = rsGraAchievementGracz.getBoolean("achievement4");
                    Boolean acz5 = rsGraAchievementGracz.getBoolean("achievement5");
                    Boolean acz6 = rsGraAchievementGracz.getBoolean("achievement6");
                    Boolean acz7 = rsGraAchievementGracz.getBoolean("achievement7");
                    Boolean acz8 = rsGraAchievementGracz.getBoolean("achievement8");
                    Boolean acz9 = rsGraAchievementGracz.getBoolean("achievement9");
                    Boolean acz10 = rsGraAchievementGracz.getBoolean("achievement10");
                    Boolean acz11 = rsGraAchievementGracz.getBoolean("achievement11");
                    Boolean acz12 = rsGraAchievementGracz.getBoolean("achievement12");

                    acziwmenty = new Achievements(nazwaGry, acz1, acz2, acz3, acz4, acz5, acz6, acz7, acz8, acz9, acz10, acz11, acz12);
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return acziwmenty;
    }
}
