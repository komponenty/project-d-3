/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.osgi.project.pomocnikgracza.GUI.internal;

import java.sql.Date;
import javax.swing.ImageIcon;

/**
 *
 * @author Bartek
 */
public class Game {
    public String NazwaGry;
    public Date DataWydania;
    public String TrybGry;
    public ImageIcon Okladka;
    public long CzasGry;
    public String NazwaProcesu;
    public String SciezkaDoSavow;

    public String getNazwaGry() {
        return NazwaGry;
    }

    public Date getDataWydania() {
        return DataWydania;
    }

    public String getTrybGry() {
        return TrybGry;
    }

    public ImageIcon getOkladka() {
        return Okladka;
    }

    public String getNazwaProcesu() {
        return NazwaProcesu;
    }
    
    
    
    public Game(String NazwaGry,Date DataWydania,String TrybGry,ImageIcon Okladka,String NazwaProcesu, String SciezkaDoSavow)
    {
     this.DataWydania = DataWydania;
     this.NazwaGry = NazwaGry;
     this.NazwaProcesu = NazwaProcesu;
     this.Okladka = Okladka;
     this.TrybGry = TrybGry;
     this.SciezkaDoSavow = SciezkaDoSavow;
    }
    
    @Override public String toString()
    { 
       return String.format("Nazwa gry: %s, data wydania: %s, rodzaj gry: %s", this.NazwaGry,this.DataWydania.toString(),this.TrybGry);
    }
}
