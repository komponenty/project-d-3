/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.osgi.project.pomocnikgracza.GUI.internal;

/**
 *
 * @author Bartek
 */
public class Achievements {
    Boolean acziwment1;
    Boolean acziwment2;
    Boolean acziwment3;
    Boolean acziwment4;
    Boolean acziwment5;
    Boolean acziwment6;
    Boolean acziwment7;
    Boolean acziwment8;
    Boolean acziwment9;
    Boolean acziwment10;
    Boolean acziwment11;
    Boolean acziwment12;
    String NazwaGry;

    public Boolean isAcziwment1() {
        return acziwment1;
    }

    public Boolean isAcziwment2() {
        return acziwment2;
    }

    public Boolean isAcziwment3() {
        return acziwment3;
    }

    public Boolean isAcziwment4() {
        return acziwment4;
    }

    public Boolean isAcziwment5() {
        return acziwment5;
    }

    public Boolean isAcziwment6() {
        return acziwment6;
    }

    public Boolean isAcziwment7() {
        return acziwment7;
    }

    public Boolean isAcziwment8() {
        return acziwment8;
    }

    public Boolean isAcziwment9() {
        return acziwment9;
    }

    public Boolean isAcziwment10() {
        return acziwment10;
    }

    public Boolean isAcziwment11() {
        return acziwment11;
    }

    public Boolean isAcziwment12() {
        return acziwment12;
    }

    public String getNazwaGry() {
        return NazwaGry;
    }
    
    public Achievements() {
        
    }
    
    public Achievements(String nazwagry,Boolean acziwment1,Boolean acziwment2,Boolean acziwment3,Boolean acziwment4,Boolean acziwment5,Boolean acziwment6,Boolean acziwment7,Boolean acziwment8,
            Boolean acziwment9,Boolean acziwment10,Boolean acziwment11,Boolean acziwment12)
    {
        this.NazwaGry = nazwagry;
        this.acziwment1 = acziwment1;
        this.acziwment2 = acziwment2;
        this.acziwment3 =  acziwment3;
        this.acziwment4 =  acziwment4;
        this.acziwment5 =  acziwment5;
        this.acziwment6 =  acziwment6;
        this.acziwment7 =  acziwment7;
        this.acziwment8 =  acziwment8;
        this.acziwment9 =  acziwment9;
        this.acziwment10 =  acziwment10;
        this.acziwment11 =  acziwment11;
         this.acziwment12 =  acziwment12;
        
    }
    
    @Override public String toString()
    {
        String s1 = Boolean.toString(this.acziwment1);
         String s2 = Boolean.toString(this.acziwment2);
          String s3 = Boolean.toString(this.acziwment3);
       return String.format("Gra: %s, czy achievement 1 jest zdobyty: %s,czy achievement 2 jest zdobyty: %s,czy achievement 3 jest zdobyty: %s, ",
               this.NazwaGry,s1,s2,s3);
       
    }
}
