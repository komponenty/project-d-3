package pk.osgi.project.pomocnikgracza.GUI.internal;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import pk.osgi.project.pomocnikgracza.Contract.*;
import pk.glk.soundplayer.ISound;
import pk.glk.soundplayer.internal.SoundPlayer;

/**
 * Extension of the default OSGi bundle activator
 */
public final class GUIActivator implements BundleActivator {
    
    ServiceReference backupRef;
    ServiceReference gamingTimeRef;
    ServiceReference screenShotRef;
    ServiceReference mailRef;
    
    IScreenShot screenShot;
    IGamingTime gamingTime;
    IBackup backup;
    IMail mail;
    
    Thread gamingTimeThread;
    
    ISound sound = new SoundPlayer();
    pakman.logger.contract.Logger logger = new pakman.logger.impl.FileLogger();
    
    List<String> listaProcesow = new ArrayList();
    
    
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    public void start( BundleContext bc ) throws Exception {
        System.out.println( "Startowanie GUI!" );
        
        backupRef = bc.getServiceReference(IBackup.class.getName());
        backup =(IBackup)bc.getService(backupRef);
        
        gamingTimeRef = bc.getServiceReference(IGamingTime.class.getName());
        gamingTime = (IGamingTime)bc.getService(gamingTimeRef);
        
        screenShotRef = bc.getServiceReference(IScreenShot.class.getName());
        screenShot = (IScreenShot)bc.getService(screenShotRef);
        
        mailRef = bc.getServiceReference(IMail.class.getName());
        mail = (IMail)bc.getService(mailRef);
        
        screenShot.SluchajKlawy();
        
        gamingTimeThread = new Thread((Runnable) gamingTime);
        gamingTimeThread.start();
        
        final DataBase dataBase = new DataBase();
        
        final NickChooser nc = new NickChooser();
        nc.setTitle("PomocnikGracza - Login");
        nc.setDataBase(dataBase);
        nc.setVisible(true);    
        
        final GUI okno = new GUI();
        okno.setBackup(backup);
        okno.setMail(mail);
        
        nc.addWindowListener(new WindowListener() {  
            @Override public void windowOpened(WindowEvent e) {}
            @Override public void windowClosing(WindowEvent e) {}
            @Override public void windowClosed(WindowEvent e) { 
                okno.setTitle("PomocnikGracza - " + nc.getNick());
                okno.setNick(nc.getNick());
                //try {
                    //dataBase.DoConnectDlaIstniejacegoUzytkownika(nc.getNick());
                    okno.setDataBase(dataBase);
                    okno.WczytanieLista_gier_gracza();
                //} catch (SQLException ex) {
                    //Logger.getLogger(GUIActivator.class.getName()).log(Level.SEVERE, null, ex);
                //}
                okno.setVisible(true);
                sound.PlaySound("sound/tada.wav", false);
            }
            @Override public void windowIconified(WindowEvent e) {}
            @Override public void windowDeiconified(WindowEvent e) {}
            @Override public void windowActivated(WindowEvent e) {}
            @Override public void windowDeactivated(WindowEvent e) {}
        });
        
        logger.WriteLine("GUI wystartowalo poprawnie!");
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop( BundleContext bc ) throws Exception {
        System.out.println( "Ladowanie GUI!" );
        screenShot.NieSluchajKlawy();
        sound.PlaySound("sound/windows_shutdown.wav", false);
        Thread.sleep(1000);
        logger.WriteLine("GUI wyladowalo poprawnie!");
    }
}

