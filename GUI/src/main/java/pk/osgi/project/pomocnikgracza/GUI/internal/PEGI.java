/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.osgi.project.pomocnikgracza.GUI.internal;

/**
 *
 * @author Bartek
 */
public class PEGI {

    public String NazwaGry;
    public Boolean Violence;
    public Boolean Language;
    public Boolean Fear;
    public Boolean Sex;
    public Boolean Drugs;
    public Boolean Discrimination;
    public Boolean Gambling;
    public Boolean Online;
    public int Wiek;

    public String getNazwaGry() {
        return NazwaGry;
    }

    public Boolean isViolence() {
        return Violence;
    }

    public Boolean isLanguage() {
        return Language;
    }

    public Boolean isFear() {
        return Fear;
    }

    public Boolean isSex() {
        return Sex;
    }

    public Boolean isDrugs() {
        return Drugs;
    }

    public Boolean isDiscrimination() {
        return Discrimination;
    }

    public Boolean isGambling() {
        return Gambling;
    }

    public Boolean isOnline() {
        return Online;
    }

    public int getWiek() {
        return Wiek;
    }
    
    public PEGI() {
        
    }
    

    public PEGI(String NazwaGry, Boolean Violence, Boolean Language, Boolean Fear,
            Boolean Sex, Boolean Drugs, Boolean Discrimination, Boolean Gambling, Boolean Online, int Wiek) 
    {
        this.NazwaGry = NazwaGry;
        this.Violence = Violence;
        this.Language = Language;
        this.Fear = Fear;
        this.Sex = Sex;
        this.Drugs = Drugs;
        this.Discrimination = Discrimination;
        this.Gambling = Gambling;
        this.Online = Online;
        this.Wiek = Wiek;
    }

    @Override
    public String toString() {
         String s1 = Boolean.toString(this.Violence);
         String s2 = Boolean.toString(this.Language);
         String s3 = Boolean.toString(this.Fear);
         String s4 = Boolean.toString(this.Sex);
         String s5 = Boolean.toString(this.Drugs);
         String s6 = Boolean.toString(this.Discrimination);
         String s7 = Boolean.toString(this.Gambling);
         String s8 = Boolean.toString(this.Online);
         
                
        return String.format("Gra: %s,Violence: %s,Language: %s,Fear: %s,Sex: %s,Drugs: %s,Discrimination: %s,Gambling: %s,Online: %s,Wiek: %s,",
                this.NazwaGry,s1,s2,s3,s4,s5,s6,s7,s8,
                Integer.toString(Wiek));
    }
}
