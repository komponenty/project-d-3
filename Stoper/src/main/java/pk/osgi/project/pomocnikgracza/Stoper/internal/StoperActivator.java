package pk.osgi.project.pomocnikgracza.Stoper.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import pk.osgi.project.pomocnikgracza.Contract.IStoper;

/**
 * Extension of the default OSGi bundle activator
 */
public final class StoperActivator implements BundleActivator {
    
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    public void start( BundleContext bc ) throws Exception {
        System.out.println( "Startowanie Stoper!" );

        //Register our example service implementation in the OSGi service registry
        bc.registerService( IStoper.class.getName(), new Stoper(), null );
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop( BundleContext bc ) throws Exception {
        System.out.println( "Ladowanie Stoper!" );
    }
}

