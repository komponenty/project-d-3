/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.Stoper.internal;

import pk.osgi.project.pomocnikgracza.Contract.IStoper;

/**
 *
 * @author MadMike
 */
public class Stoper implements IStoper {
    
    private long poczatek;
    private long koniec;
    long czas;
    private boolean pracuje = false;
    
    public Stoper() { 
    
    }
    
    public boolean getPracuje() {
        return pracuje;
    }
    
    public long getCzas() {
        return czas;
    }
    
    @Override
    public void Start() {
        // w milisekundach
        poczatek = System.currentTimeMillis();
        pracuje = true;
    }
    
    @Override
    public long Stop() {
        // w milisekundach
        koniec = System.currentTimeMillis();
        // zwraca wynik w sekundach
        czas = (koniec - poczatek)/1000;
        pracuje = false;
        return czas;
        
    }
}
    
