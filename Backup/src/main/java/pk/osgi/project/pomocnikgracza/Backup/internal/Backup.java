package pk.osgi.project.pomocnikgracza.Backup.internal;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import org.apache.commons.io.FileUtils;
import pk.osgi.project.pomocnikgracza.Contract.IBackup;

public class Backup implements IBackup {
    
    private File[] savy;
    private File[] moje_savy;
    private DefaultListModel model = new DefaultListModel();
    private DefaultListModel model2 = new DefaultListModel();
    private String folder_gry = "C:\\Users\\";
    private String folder_gracza = "C:\\Users\\";
    
    public Backup() {
        
    }

    public void setFolder_gry(String folder_gry) {
        this.folder_gry = folder_gry;
    }

    public void setFolder_gracza(String folder_gracza) {
        this.folder_gracza = folder_gracza;
    }
  
    @Override
    public File[] finder( String dirName){
    	File dir = new File(dirName);

    	return dir.listFiles(new FilenameFilter()
        {
             @Override
             public boolean accept(File dir, String filename)
                  { 
                      return filename.contains("."); 
                  }
    	} );

    }
    
    @Override
    public void aktualizacja_listy_prawej(JList listbox)
    {
        moje_savy = finder(folder_gracza);
        List<File> lista_z_tablicy = Arrays.asList(moje_savy);
        lista_z_tablicy = new ArrayList(lista_z_tablicy);
        model2 = new DefaultListModel(); //zerowanie modelu
             for (int i = 0; i < lista_z_tablicy.size(); i++) {
                 model2.addElement(lista_z_tablicy.get(i).getName());
             }
        listbox.setModel(model2);
    }
    
    @Override
    public void aktualizacja_listy_lewej(JList listbox)
    {
        savy = finder(folder_gry);
        List<File> lista_z_tablicy = Arrays.asList(savy);
        lista_z_tablicy = new ArrayList(lista_z_tablicy);
        model = new DefaultListModel(); //zerowanie modelu
             for (int i = 0; i < lista_z_tablicy.size(); i++) {
                 model.addElement(lista_z_tablicy.get(i).getName());
             }
        listbox.setModel(model);
    }
    
    @Override
    public void kopiowanie_pliku_w_lewo(JList listbox)
    {
        //plik który chcemy kopiować
        String nazwa_sejwa = listbox.getSelectedValue().toString(); 
        
        //sprawdzamy czy plik już istnieje
        if (model.contains(nazwa_sejwa)) {
            int reply = JOptionPane.showConfirmDialog(null, "Plik o takiej nazwie ju� istnieje, zast�pi� ?", "", JOptionPane.YES_NO_OPTION);
            if (reply != JOptionPane.YES_OPTION) {
                return;
            }
        }
        
        //pełna ścieżka tego co chcemy kopiować
        File source = new File(folder_gracza + nazwa_sejwa); 
        
        //folder do którego chcemy kopiować
        File desc = new File(folder_gry); 
        
        try {
            FileUtils.copyFileToDirectory(source, desc); //proces kopiowania
        } catch (IOException ex) {
            Logger.getLogger(Backup.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void kopiowanie_pliku_w_prawo(JList listbox)
    {
        //plik który chcemy kopiować
        String nazwa_sejwa = listbox.getSelectedValue().toString(); 
        
        //sprawdzamy czy plik już istnieje
        if (model2.contains(nazwa_sejwa)) {
            int reply = JOptionPane.showConfirmDialog(null, "Plik o takiej nazwie ju� istnieje, zast�pi� ?", "", JOptionPane.YES_NO_OPTION);
            if (reply != JOptionPane.YES_OPTION) {
                return;
            }
        }
        
        //pełna ścieżka tego co chcemy kopiować
        File source = new File(folder_gry + nazwa_sejwa); 
        
        //folder do którego chcemy kopiować
        File desc = new File(folder_gracza); 
        
        try {
            FileUtils.copyFileToDirectory(source, desc); //proces kopiowania
        } catch (IOException ex) {
            Logger.getLogger(Backup.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
