package pk.osgi.project.pomocnikgracza.Backup.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.osgi.project.pomocnikgracza.Contract.IBackup;


/**
 * Extension of the default OSGi bundle activator
 */
public final class BackupActivator implements BundleActivator {
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    public void start( BundleContext bc ) throws Exception {
        System.out.println("Startowanie Backup!");

        // Register our example service implementation in the OSGi service registry
        bc.registerService( IBackup.class.getName(), new Backup(), null );
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop( BundleContext bc ) throws Exception {
        System.out.println("Ladowanie Backup!");
    }
}

