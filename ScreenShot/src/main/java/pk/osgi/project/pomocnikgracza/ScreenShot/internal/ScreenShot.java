/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.osgi.project.pomocnikgracza.ScreenShot.internal;

import de.ksquared.system.keyboard.GlobalKeyListener;
import de.ksquared.system.keyboard.KeyAdapter;
import de.ksquared.system.keyboard.KeyEvent;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import pk.osgi.project.pomocnikgracza.Contract.IScreenShot;


/**
 *
 * @author MadMike
 */
public class ScreenShot implements IScreenShot {
    
    GlobalKeyListener gkl;
    KeyAdapter ka;
    
    public ScreenShot(GlobalKeyListener gkl) {
        this.gkl = gkl;
    }
    
    @Override
    public void SluchajKlawy() {
        
        ka = new KeyAdapter() {
 
            @Override public void keyPressed(KeyEvent event) {
            }
            
            @Override public void keyReleased(KeyEvent event) {
                if (event.getVirtualKeyCode() == 44 || event.getVirtualKeyCode() == 154) {
                    try {
                        ZapiszScreenShot();
                    } catch (AWTException ex) {
                        Logger.getLogger(ScreenShot.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ScreenShot.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        gkl.addKeyListener(ka);
    }
    
    @Override
    public void NieSluchajKlawy() {
        gkl.removeKeyListener(ka);
    }
    
    @Override
    public void ZapiszScreenShot() throws AWTException, IOException {
        Robot robot = new Robot();
        
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        
        Rectangle rectangle = new Rectangle(dimension);
        
        BufferedImage image = robot.createScreenCapture(rectangle);
        
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String data = df.format(new Date());
        
        String username = System.getProperty("user.name");
        File f = new File("C:\\Users\\" + username + "\\Desktop\\PomocnikGracza\\ScreenShots\\");
        if (!f.exists()) {
            f.mkdirs();
        }
        ImageIO.write(image, "png", new File("C:\\Users\\" + username + "\\Desktop\\PomocnikGracza\\ScreenShots\\"+ data +".png"));
    }
}
