package pk.osgi.project.pomocnikgracza.ScreenShot.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import de.ksquared.system.keyboard.GlobalKeyListener;
import pk.osgi.project.pomocnikgracza.Contract.IScreenShot;


/**
 * Extension of the default OSGi bundle activator
 */
public final class ScreenShotActivator implements BundleActivator {
    
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    public void start( BundleContext bc ) throws Exception {
        System.out.println("Startowanie ScreenShot!");
        
        GlobalKeyListener gkl = new GlobalKeyListener();
        ScreenShot ss = new ScreenShot(gkl);
        
        // Register our example service implementation in the OSGi service registry
        bc.registerService(IScreenShot.class.getName(), ss, null);
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop( BundleContext bc ) throws Exception {
        System.out.println("Ladowanie ScreenShot!");
    }
}

